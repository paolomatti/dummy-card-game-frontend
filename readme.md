# Dummy Card Game

## Installation
In order to run the project you need to have the parcel-bundler installed on your machine.
To do so, just run the following command:

```bash
npm install -g parcel-bundler
```

This will install the bundler globally on your machine.
(and obviously run `npm-install` so that you can get your node_modules working properly).