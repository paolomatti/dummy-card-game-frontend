import * as React from 'react';
import ReactDOM from 'react-dom';

const App: React.FC = () => (
    <div className="app">
        Project has been set up.
    </div>
)

ReactDOM.render(<App />, document.getElementById("root"));